--Pre-requisites:
--1) install ghc
--2) install cabal, then `cabal install random` to get the random package

-- To compile and run:
-- $ ghc makeInsertFile && ./makeInsertFile

-- Generate a file of random SQL inserts of the form:
-- id = random
-- other_id =  taken from a random pool of 1000, generated up front
--
-- Illustrates how to work with a reusable set of random numbers, stored in an array.

import System.Random (newStdGen, randomRs, randomRIO)
import Control.Monad (replicateM)
import Data.Array
import Control.Applicative (liftA2)

type AIS = Array Int String

outFile = "insert.sql"
table = "dummy"
schema = "BEN_TEST"
numberOfInserts = 1000000

-- Number of distinct other_id values
nOtherIds :: Int
nOtherIds = 1000

randId :: IO String
randId = fmap (take 16 . randomRs ('a','z')) newStdGen

randIdx :: IO Int
randIdx = randomRIO (0, nOtherIds - 1)

otherIds :: IO AIS
otherIds = fmap (listArray (0, nOtherIds - 1)) $ replicateM nOtherIds randId

makeOneInsert :: (String, String) -> String
makeOneInsert (id, otherId) = "INSERT INTO " ++ schema ++ "." ++ table ++ " (id,other_id) VALUES ('" ++ id ++"','" ++ otherId ++ "');"

-- Take n random elements from the array l
randElems :: Int -> Array Int a -> IO [a]
randElems n l = replicateM n $ fmap (l !) randIdx

-- Get n other_ids, taken from the available random otherIds
randOtherIds :: Int -> IO [String]
randOtherIds n = otherIds >>= (randElems n)

randIds :: Int -> IO [String]
randIds n = replicateM n randId

zipM :: Applicative f => f [String] -> f [String] -> f [(String,String)]
zipM = liftA2 zip

bothIds :: Int -> IO [(String,String)]
bothIds n = zipM (randIds n) (randOtherIds n)

writeLines :: Int -> IO [String]
writeLines n = fmap (map makeOneInsert) (bothIds n)

main :: IO()
main = (writeLines numberOfInserts) >>= ((writeFile outFile) . unlines)
